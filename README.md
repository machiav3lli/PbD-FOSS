## What's this

This is an intial documentation of the Privacy by Design guideline for FOSS, developed with the FOSS community.

Links to the full paper would follow when published.

The code is hosted at [GitLab](https://gitlab.com/machiav3lli/PbD-FOSS), so you can gladly drop me an issue if you discover some errors or have some more specific questions.
