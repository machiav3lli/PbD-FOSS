---
date: 2024-02-20
description: "Complementary Rights"
tags: ["rights"]
weight: 10
title: "Complementary Rights"
---

Many people would argue that rights are all equal and that there should be no discussion about the priority of human rights won through long struggles; more specifically, we're not trying to discard any rights, but rather to sharpen the focus on a specific set of rights that the community considers essential, reducing the noise for FOSS developers who want to get involved with PbD guidelines but don't know where to start. So here's a complementary list of documented privacy rights, and a few suggested by participants in the study.

## Restriction of Processing:

The right to restrict the processing of personal data allows individuals to limit the use of their information under certain circumstances, providing them with control over how their data is handled

## Non-Discrimination Based on Identity and Values:

This right ensures that individuals are not discriminated against based on their identity, beliefs, or personal characteristics, safeguarding their privacy and ensuring fair treatment

## Education on Privacy Choices:

Individuals have the right to be well-informed about their privacy options, empowering them to make informed decisions about how their personal information is used and shared

## Data Portability:

The right to data portability enables individuals to obtain and reuse their personal data across different services, enhancing control over their information through standardized formats

## Objection to Automated Decision-Making:

This right allows individuals to challenge decisions made solely by automated processes without human intervention, ensuring transparency and accountability in decision-making processes

## Rectification of Own Data:

Individuals have the right to correct inaccuracies in their personal data, enabling them to ensure that the information held about them is accurate and up-to-date

## Choice After Payment:

This right grants individuals the option to make choices even after a transaction has been completed, particularly relevant in scenarios like gaming where additional choices may be offered post-payment

## Right to Complain:

Individuals have the right to lodge complaints regarding the handling of their personal data, ensuring accountability and enforcement of privacy regulations.
