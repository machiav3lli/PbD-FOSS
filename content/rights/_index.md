---
title: "Privacy Rights"
date: 2017-03-02T12:00:00-05:00
---

{{< button "./essential_rights" "Essential Privacy Rights" "mb-1" >}}

{{< button "./complementary_rights" "Complementary Privacy Rights" >}}
