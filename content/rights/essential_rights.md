---
date: 2023-08-16
description: "Essential Rights"
tags: ["discussion"]
weight: 1
title: "Essential Rights"
---

## Erasure (Right to be Forgotten)

The right to erasure, also known as the right to be forgotten, allows individuals to request the deletion or removal of their personal data when there is no compelling reason for its continued processing. This right empowers individuals to have control over their personal data and is particularly relevant in cases where the data is no longer necessary for its original purpose or where consent is withdrawn.

Comments: Generally considered important, but can be abused in certain sensitive contexts, such as government data.

## Individuals Access

This right gives individuals the ability to obtain confirmation from a data controller as to whether or not their personal data are being processed and, if so, to access those data and receive additional information about the processing. It enables individuals to verify the lawfulness of data processing and to understand how their data is being used.

Comments: Considered essential and already enshrined in the GDPR, but quite difficult to apply when people don't formally have an account (e.g. automated profiling).

## Right to be Informed.

The right to be informed is designed to ensure that individuals are aware of the ways in which their personal data is being used. This includes the purposes of the data processing, the identity of the data controller, and any third parties with whom the data may be shared. Transparency in data processing is essential for building trust and enabling individuals to make informed choices about their personal data.

Comments: A fairly essential right, but complex to fully enforce or get right without some UX costs.

## Right to Withdraw Consent

Individuals have the right to withdraw consent to the processing of their personal data at any time. This right should be as easy to exercise as giving consent, and individuals should be informed of this right before giving consent. Withdrawal of consent should not affect the lawfulness of the processing based on the consent prior to its withdrawal.

Comments: A fundamental privacy right, but difficult to enforce, especially since users can't control these mostly opaque entities.

## Right to Object (Data Processing and Retention)

The right to object allows individuals to object to the processing of their personal data, including its retention, on grounds relating to their particular situation. This right must be respected unless the data controller can demonstrate compelling legitimate grounds for the processing that override the interests, rights and freedoms of the individual.

Comments: An essential right that is difficult to enforce in some use cases (e.g. contacts in Whatsapp).

## Not to be Discriminated Against (Based on Privacy Choices)

Individuals have the right not to be discriminated against on the basis of their privacy choices. This means that exercising privacy rights, such as opting out of data processing or refusing to consent to certain uses of personal data, should not result in unfair treatment or negative consequences.

Comments: Essential for the exercise of fundamental rights, but may still be an issue in certain use cases (e.g. banking and payment trust).


## Right to Object Marketing

Individuals have the right to object to the processing of their personal data for direct marketing purposes, including profiling for such marketing purposes. If this right is exercised, the personal data should no longer be processed for marketing purposes.

Comments: It's mostly considered essential, and while that's pretty understandable from a user perspective, it's a funding challenge for some developers.

## For Individuals to Exercise Their Rights

This right ensures that individuals are able to exercise their privacy rights effectively. It includes providing clear and easily accessible information about how to exercise those rights, and facilitating mechanisms for making requests and receiving timely responses.

Comments: Essential and associated with the right to be informed.

## Restriction of Processing

This right allows individuals to request that the processing of their personal data be restricted in certain circumstances. If processing is restricted, the data may only be stored and processed with consent or for legal claims, to protect the rights of another person, or for reasons of important public interest.

Comments: Important, but hard to get right when most users don't fully understand what processing can mean.
