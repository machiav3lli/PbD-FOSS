---
title: "Privacy by Design Guideline for FOSS"

description: "Documentation of Privacy by Design principles and core privacy rights for FOSS developers, as identified in an HCD study."
---

This is an intial documentation of the Privacy by Design (PbD) guideline for FOSS, developed with the FOSS community in a Human Centred Design (HCD) study.

This website aims mainly to allow interested persons to navigate the resulting PbD guideline model and comprehend what the principles look like in application, without having to read the full paper (To be linked).

The code is hosted at [GitLab](https://gitlab.com/machiav3lli/PbD-FOSS), so you can gladly drop me an issue if you discover some errors or have some more specific questions.

## Related Information

### FOSS Backstage: Single Developer Community? About community architecture.

Daniel Guagnin, I and Anett Hübner will be [talking at FOSS-Backstage](https://program.foss-backstage.de/fossback24/talk/LLAFBT/) about FOSS community build and maintainance.

### Study on "Privacy Visualization" for Neo Store, the F-Droid client

This study is a follow up to a study on privacy visualization for Neo Store, conducted similarly based Human Centered Design (HCD). A similar documentation for that study is accessible at [https://machiav3lli.gitlab.io/Neo-Privacy-Visualization/](https://machiav3lli.gitlab.io/Neo-Privacy-Visualization/), you may also head directly to the [full paper](https://dx.doi.org/10.17169/refubium-37861).


