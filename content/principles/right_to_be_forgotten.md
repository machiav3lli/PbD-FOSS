---
date: 2024-02-20
description: "Right to be forgotten"
tags: ["pricniples"]
weight: 9
title: "Right to be forgotten"
---

## Right to be forgotten

The right to be forgotten emphasizes the right of users to request the removal of all personal information.

It requires developers to consider deleting or removing personal data when an individual withdraws consent. By incorporating this principle at the design stage, FOSS developers can ensure that privacy is a core consideration from the outset, rather than an afterthought.

This approach not only increases individuals' control over their personal information, but also fosters trust and transparency in data processing practices. Embracing the right to be forgotten empowers the FOSS community to uphold privacy rights and comply with regulatory requirements, ultimately contributing to a more privacy-respecting digital ecosystem.

Public interest is one of the considerations in implementing this principle, for example when working with official government data.

### Example

For example, a social media platform can proactively design its systems to automatically delete user data after a period of inactivity or at the user's request. By incorporating this feature into the design of the platform, the company not only complies with regulations, but also demonstrates a commitment to privacy by considering data deletion as an inherent part of the normal data lifecycle.

This approach prioritizes privacy considerations throughout the development and operation of the platform, ultimately enhancing user control.
