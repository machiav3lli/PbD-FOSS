---
date: 2024-02-20
description: "Complementary PbD Principles"
tags: ["pricniples"]
weight: 12
title: "Complementary PbD Principles"
---

As with privacy rights, we don't consider any PbD principle to be obsolete or discardable; rather, we're trying to sharpen the focus on relevant principles and help FOSS developers work with PbD easily.

So here's a complementary list of documented PbD principles, and a few suggested by participants in the study.

## Functionality

It should be ensured that privacy is seamlessly integrated into products and services, making it a core part of the user experience without forcing users to choose between functionality and privacy.

## Disclosure

This principle emphasizes openness with users about privacy policies and procedures, through clear communication on the attitude of the service provider toward requests from law enforcement.

## Sale

In the context of Privacy by Design, the Sale principle focuses on respecting user privacy by giving individuals control over their data, ensuring that companies handle personal information safely and securely.

## Dealing with Unsolicited Personal Data

This principle emphasizes providing necessary safeguards against unsolicited personal data collection.

## Education (suggested in the interviews)

This principle emphasizes the importance of incorporating privacy principles with the goal of educating users in an easy, clear way of data processing.

## UX Qualitative Development without Data Collection (suggested in the interviews)

This principle encourages a privacy-first mindset in UX development, where every design decision prioritizes both functionality and privacy. This is mainly possible through participative development, actively engaging the users in development choices.

## Data Loss Precautions (suggested in the interviews)

This principle ensures full lifecycle protection of data, with security measures in place to specifically prevent data loss.

## Accountability

A key principle, requiring companies to document actions clearly, transparently communicate privacy practices, appointing a legally accountable person. This is a part of GDPR and other privacy regulations. That said FOSS licenses, may free developers from responsibility of own software, but not of accountability on personal data management.

## Share

This principle emphasizes putting the power in users' hands to manage if any of the collected data should leave the ownership of the service provider.

## Treating Personal Data as Part of Our Lives (suggested in the interviews)

This principle underscores the importance of making privacy a fundamental consideration in how personal data is handled and protected, getting similar handling to the fundamental right to live.

## Correctness

This ensures that personal data is accurate, complete and up-to-date throughout its lifecycle. Empowering users to correct own personal data is one of the approaches to do so.

## Adoption, Use or Disclosure of an Identifier

This principle guides developers on if and how they adopt, use, or disclose unique identifiers while respecting user privacy rights and ensuring compliance with relevant laws.

## Cross-Border Transfer

This principle aims to address the challenges of cross-border data transfer by implementing measures to protect personal information during international exchanges and whether this should be allowed at all.

## Lawfulness

This principle emphasizes compliance with legal requirements when handling personal data to ensure that data processing activities are conducted within legal boundaries.

## Assistance for Users (suggested in the interviews)

This includes providing assistance to users in form of self-help documentation, issue forums or community channels.

## Source

This principle emphasizes the need to focus on the source of personal information and that personal information must come from its owner.
