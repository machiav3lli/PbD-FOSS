---
date: 2024-02-20
description: "Adoption, Use or Disclosure of an identifier"
tags: ["pricniples"]
weight: 1
title: "Adoption, Use or Disclosure of an identifier"
---

## Adoption, Use or Disclosure of an identifier

This principle emphasizes the need to prevent the creation, collection, or use of personal identifiers for multiple purposes. By adhering to this prohibition, FOSS developers can significantly reduce the risk of data breaches and unauthorized access to sensitive information.

This approach is fully consistent with the principles of data minimization and purpose limitation, ensuring that only the minimum amount of personal data necessary for a particular purpose is processed.

Ultimately, incorporating this prohibition into design practices fosters a safer and more privacy-respecting environment for individuals and increases overall trust in data handling processes.

### Example

One example is the implementation of tokenization in payment systems: When a customer makes a purchase, their credit card number is replaced with a unique token. This token is used to process and store the transaction, while the actual credit card number is not retained by the merchant.

By prohibiting the use and disclosure of the original credit card number, this approach minimizes the risk of unauthorized access to sensitive financial information and ensures that the purposes for which the two identifiers are used are completely separate.
